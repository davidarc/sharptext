var app = angular.module('forgottenPasswordModule', []);

app.controller('forgottenPasswordCtrl', ["$scope", "$firebase", "$location", "firebaseRef", function ($scope, $firebase, $location, firebaseRef) {
  
	var ref = firebaseRef.getDB();

   	$scope.user = {};
 	$scope.user.email = "";

 	$scope.submitForm = function(isValid) {
 		if (isValid) {
 			ref.resetPassword({
    			email : $scope.user.email
  				}, 
  				function(error) {
  					if (error === null) {
						console.log("Password reset email sent successfully");
						alert("Ett meddelande för lösenordsåterställning skickas till: "+ $scope.user.email + ". Glöm inte att kolla i dina 'spam/trash' mapperna också! Använd det tillfälliga lösenordet i meddelandet för att logga in och återställa dit lösenord.");
						$scope.$apply(function() {
  							$location.path('/login');
  						});						
					} else {
						console.log("Error sending password reset email:", error);
						$scope.showError = true;
						$scope.sendEmailError = "Kunde inte skicka meddelandet:" + error;
					}  					
  				}
  			);
 		}
 	};

}]);