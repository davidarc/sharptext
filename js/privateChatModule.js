var app = angular.module("privateChatModule",[]);

//initialization after all the modules have been loaded
//calling on("deviceready") and performing work here guarantees that ionic and the device libraries have been loaded before
//we try to access any device APIs/plugins
//use $ionicPlatform.on("cordova event", callbackFn) to be sure that ionic doesn't mess anything up  
app.run(function($ionicPlatform, $window, $location){
	$ionicPlatform.on("deviceready", function(){
		
	});
});

app.controller("privateChatCtrl",["$scope", "$location", "firebaseRef", "$firebaseArray", "$firebaseObject" ,"channelID", "authUser",
"$window", "$ionicPlatform", "$ionicScrollDelegate", "$anchorScroll", "$timeout", "$document", "privateChannelID",
 function($scope, $location, firebaseRef, $firebaseArray, $firebaseObject, channelID, authUser, $window, $ionicPlatform, $ionicScrollDelegate, $anchorScroll, $timeout, $document, privateChannelID){
 	console.log("private channel id: " + privateChannelID.id);	

	$scope.scrollToBottom = function (){
		console.log("in scrollToBottom");
		console.log("scroll height: " + document.body.scrollHeight);
		$ionicScrollDelegate.scrollBottom();
		window.scrollTo(0, document.body.scrollHeight);
  	};

 	
	$scope.alias = "";
	$scope.message= "";

	// Firebase reference
	var ref = firebaseRef.getDB();
	$scope.messages= $firebaseArray(ref.child("messages/"+privateChannelID.id));

	$scope.messages.$loaded().then(function() {
		console.log("In load messages");
		var queryResult = $document[0].querySelector('#allContent'); 
		queryResult.scrollTop = queryResult.scrollHeight;
		console.log("height after load: " + queryResult.offsetHeight);
	});

	// user ID reference
	var userId= authUser.userId;
	var userRef= ref.child("users/"+userId);
	
	var userObject = $firebaseObject(userRef);
	// Firebase values are loaded asynchronously. Therefore if we need to use the values in our code we need to call 
	// $loaded to get a promise which ensures that our code is only executed once the data from firebase is actually loaded. 
	userObject.$loaded().then(function () {
		$scope.userName =  userObject.displayName;	
	});

	$scope.inputMessage="";
	$scope.date="";

	$scope.keyCodeCheck = function(event){
		if(event.keyCode === 13){
			console.log("keyCodeCheck reached!");
			$scope.sendButton();
		}
	};
		
	$scope.sendButton = function() {	
		$scope.messageRef = ref.child("messages/"+privateChannelID.id);
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var hour = today.getHours();
		var min = today.getMinutes();
		var sec = today.getSeconds();

		if(dd<10) {
		    dd='0'+dd
		} 
		if(mm<10) {
		    mm='0'+mm
		} 
		if(hour<10) {
		    hour='0'+hour
		} 
		if(min<10) {
		    min='0'+min
		} 
		if(sec<10) {
		    sec='0'+sec
		} 
		today = dd+'/'+mm+" Kl."+hour+":"+min;
		$scope.messageRef.push({alias:$scope.userName, message:$scope.inputMessage, date:today});
		// Scrolls to the last message when a message is added.
		$ionicScrollDelegate.scrollBottom(true);
		$scope.inputMessage="";		
	}


	$scope.getBack = function(){
		$window.history.back();
	}

					
}]);


